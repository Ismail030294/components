---
title: StrategyCard
description: A UI card showcasing a strategy. 
position: 1
category: Guide
---

This UI card component showcases a strategy. It has a large illustration and a call to action button to accompany the title and description.

## Design 

<cta-button link="https://www.figma.com/file/2gSU3IRPFTSquJg8RrRhMK/?node-id=1303%3A2225" text="Figma Page"></cta-button>

## Vue + CSS

<strategy-card-css></strategy-card-css>

## Vue + Tailwind

_Coming soon_